package com.example.student1.shifr;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button plus, minus, ok;
    TextView TEXT, answer, number;
    int num=0;
    int count=0;
    String ans="";
    char[] character={'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й',
                      'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф',
                      'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я',};
    int alp(char ch){
        int j=0;
    for( j=0; j<character.length; j++){
        if(ch==character[j]){
            ch=character[j];
            break;
        }

    }
        return j;
    }

    String text="Ашбт бш ьбёшдшежшё, фжчшё яь рёв дуфвёушёп бу хульи аульбуи! Ао бш вёчуша ьи ыуюуыкьюж!\n" +
        "Хцчлсн ср киъ рциучхд ъ мчъычсцъыкихс шщчлщиххсъыи. Сю кънлч ыщс, с щирьхнныъз ёыч: фнце, цнынщшнфскчъые с лчщмдцз.\n" +
        "Пятрсн эцёцат шьс анш, обсаь яьэюьпьфснай трь обста яшщьыыич ш ыняцщцл эяцгьэна, шьаьюич хынта, рст пи фцптат.\n" +
        "Йцрн ёа ж Java ийоцчжнчйрбту хеёучере цёухпе сшцухе, ёурбэнтцчжу фхузхесс ёа шиердрн цесн цйёд фхн фйхжус лй мефшцпй.\n" +
        "Юклх ыкюьз эыщ лвищ шбфдзы ийзьйщёёвйзыщжвш: лю, жщ дзлзйфю ечэв ыкя ыйюёш ймьщчлкш, в лю, дзлзйфю жвдлз жю вкизехбмюл.";
    String[] text1=text.split("\n");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        plus=(Button) findViewById(R.id.plus);
        minus=(Button) findViewById(R.id.minus);
        ok=(Button) findViewById(R.id.ok);
        TEXT=(TextView) findViewById(R.id.text);
        answer=(TextView) findViewById(R.id.answer);
        number=(TextView) findViewById(R.id.number);
        plus.setOnClickListener(this);
        minus.setOnClickListener(this);
        ok.setOnClickListener(this);
        TEXT.setText(text1[count]);
        number.setText(num+"");
        answer.setText("");
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.plus : num++; number.setText(num+"");  break;
            case R.id.minus : num--; number.setText(num+"");  break;
            case R.id.ok : count++;  num=0; number.setText(num+""); break;
        }


        if(num>32) num=32;
        if(num<0) num=0;
        number.setText(num+"");
        if(count>4) count=0;
        TEXT.setText(text1[count]);
        char[] ch=text1[count].toLowerCase().toCharArray();
        for(int i=0; i<ch.length; i++){
            if(ch[i]>=1072 && ch[i]<=1103 || ch[i]==1105){
                //if (ch[i]==1105) ch[i]=1077;

                if(alp(ch[i])+num<33) {
                    ch[i] = character[alp(ch[i])+num];
                }
                else if(alp(ch[i])+num>=33) {
                    ch[i] = character[alp(ch[i]) + num - 33];
                }

                    else if(ch[i]>1103-num){
                    ch[i]=character[ch[i]-1104+num];

                }


                //if(!(ch[i]>=1072 && ch[i]<=1103 || ch[i]==1105)) ch[i]=' ';
                }
        }
        ans= new String(ch);
        answer.setText(ans);
    }

}
